#ifndef GAPBUFFERH
#define GAPBUFFERH

typedef struct GapBuffer GapBuffer;
struct GapBuffer {
    char data[64];
    int gap_start;
    int gap_end;
    int length;
    int max_length;
};

void init_buffer(GapBuffer *buf);
void expand(GapBuffer *orig, GapBuffer *buf);
void cursor_forward(GapBuffer *buf);
void cursor_backward(GapBuffer *buf);
void move_cursor(GapBuffer *buf, int pos);
void insert_char(GapBuffer *buf, char c);
void backspace(GapBuffer *buf);
void del_next(GapBuffer *buf);
int inside_gap(GapBuffer *buf, int pos);

#endif
