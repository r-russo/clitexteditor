#include "gapbuffer.h"

void init_buffer(GapBuffer *buf) {
    buf->gap_start = 0;
    buf->gap_end = sizeof(buf->data)/sizeof(buf->data[0]) - 1;
    buf->max_length = sizeof(buf->data)/sizeof(buf->data[0]);
    buf->length = 0;
}

void expand(GapBuffer *orig, GapBuffer *buf);

void cursor_forward(GapBuffer *buf) {
    buf->data[buf->gap_start] = buf->data[buf->gap_end + 1];
    buf->gap_start++;
    buf->gap_end++;
}

void cursor_backward(GapBuffer *buf) {
    buf->data[buf->gap_end] = buf->data[buf->gap_start - 1];
    buf->gap_start--;
    buf->gap_end--;
}

void move_cursor(GapBuffer *buf, int pos) {
    if (pos < buf->gap_start) {
        while (buf->gap_start != pos) {
            cursor_backward(buf);
        }
    }
    else if  (pos > buf->gap_start) {
        while (buf->gap_start != pos) {
            cursor_forward(buf);
        }
    }
}

void insert_char(GapBuffer *buf, char c) {
    buf->data[buf->gap_start++] = c;
}

void backspace(GapBuffer *buf) {
    if (buf->gap_start > 0)
        buf->gap_start--;
}

void del_next(GapBuffer *buf) {
    cursor_forward(buf);
    backspace(buf);
}

int inside_gap(GapBuffer *buf, int pos) {
    return pos >= buf->gap_start && pos <= buf->gap_end;
}
