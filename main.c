#include <ncurses.h>
#include <stdlib.h>

#include "gapbuffer.h"

// Keycodes
#define RETURN 10
#define ESCAPE 27
#define BACKSPACE 127
#define DELETE 74

enum modes {
    NORMAL,
    COMMAND,
    INSERT
} mode;

typedef struct Cursor Cursor;
struct Cursor {
    long x;
    long y;
};

bool running = true;

int row, col;  // current col/row
int height, width;  // screen size
int saved_row, saved_col;  // saves position after executing command
Cursor cursor;
Cursor old_cursor;
GapBuffer *arr_buf;
GapBuffer cmd_buf;
int lines = 0;

void normal_mode(char key);
void command_mode(char key);
void insert_mode(char key);
int cursor_to_gaps(int cursor, GapBuffer *buf);
int gaps_to_cursor(int cursor, GapBuffer *buf);
int cursor_inside_gap(int cursor, GapBuffer *buf);

int main() {
    arr_buf = malloc(1 * sizeof(*arr_buf));
    init_buffer(&arr_buf[lines++]);
    initscr();
    raw();
    // keypad(stdscr, true);

    while(running) {
        refresh();
        getyx(stdscr, row, col);
        getmaxyx(stdscr, height, width);
        noecho();

        // draw screen
        move(0, 0);
        int pos;
        for (int i = 0; i < height - 1; i++) {  // iter every line
            if (i < lines) {  // line exists in buffer
                pos = 0;
                move(i, 0);
                clrtoeol();
                for (int j = 0; j < arr_buf[i].max_length; j++) {
                    if (!inside_gap(&arr_buf[i], j)) {
                        mvaddch(i, pos++, arr_buf[i].data[j]);
                    }
                }
            }
            else {
                mvaddch(i, 0, '~');
            }
        }

        // debug
        mvprintw(0, width - 25, "cursor = (%2d, %2d)", cursor.x, cursor.y);
        mvprintw(1, width - 25, "old cursor = (%2d, %2d)",
                old_cursor.x, old_cursor.y);
        mvprintw(2, width - 25, "lines = %4d", lines);
        mvprintw(3, width - 25, "line length = %4d", arr_buf[cursor.y].length);
        mvprintw(3, width - 25, "max length = %4d", arr_buf[cursor.y].max_length);

        // move cursor to current location
        move(cursor.y, cursor.x);

        char key = getch();

        // Normal mode
        if (mode == NORMAL) {
            normal_mode(key);
        }

        // Command mode
        else if (mode == COMMAND) {
            command_mode(key);
        }

        // Insert mode
        else if (mode == INSERT) {
            insert_mode(key);
        }

    }
    endwin();

    return 0;
}

void normal_mode(char key) {
    // current buffer index
    switch (key) {
        case RETURN:
            cursor.x++;
            break;
        case ':':
            old_cursor = cursor;
            move(height - 1, 0);
            clrtoeol();
            printw(":");
            cursor.x = 1;
            cursor.y = height - 1;
            mode = COMMAND;
            init_buffer(&cmd_buf);
            break;
        case 'i':
            mode = INSERT;
            move_cursor(&arr_buf[cursor.y], cursor.x);
            attron(A_BOLD);
            mvprintw(height - 1, 0, "-- INSERT --");
            attroff(A_BOLD);
            break;
        case 'a':
            mode = INSERT;
            move_cursor(&arr_buf[cursor.y], ++cursor.x);
            attron(A_BOLD);
            mvprintw(height - 1, 0, "-- INSERT --");
            attroff(A_BOLD);
            break;
        case 'o':
            mode = INSERT;
            attron(A_BOLD);
            mvprintw(height - 1, 0, "-- INSERT --");
            attroff(A_BOLD);
            cursor.x = 0;
            cursor.y++;
            arr_buf = realloc(arr_buf, ++lines * sizeof(*arr_buf));
            for (int i = lines - 1; i > cursor.y; i--) {
                arr_buf[i] = arr_buf[i - 1];
            }
            init_buffer(&arr_buf[cursor.y]);
        case 'h':
            if (cursor.x > 0) {
                cursor.x--;
            }
            break;
        case 'j':
            if (cursor.y < lines - 1) {
                cursor.y++;
                if (arr_buf[cursor.y].length == 0) {
                    cursor.x = 0;
                } else if (cursor.x > arr_buf[cursor.y].length - 1) {
                    cursor.x = arr_buf[cursor.y].length - 1;
                }
            }
            break;
        case 'k':
            if (cursor.y > 0) {
                cursor.y--;
                if (arr_buf[cursor.y].length == 0) {
                    cursor.x = 0;
                } else if (cursor.x > arr_buf[cursor.y].length - 1) {
                    cursor.x = arr_buf[cursor.y].length - 1;
                }
            }
            break;
        case 'l':
            if (cursor.x < arr_buf[cursor.y].length - 1) {
                cursor.x++;
            }
            break;
    }
}

void command_mode(char key){
    getyx(stdscr, row, col);
    switch(key) {
        case RETURN:
            move(height - 1, 0);
            clrtoeol();
            insert_char(&cmd_buf, '\0');  // end of string

            // Copy string inside gaps to tmp
            // Try to use a function instead
            char *tmp;
            tmp = malloc(cmd_buf.length * sizeof(*tmp));
            int ix = 0;
            for (int i = 0; i < cmd_buf.max_length; i++) {
                if (!inside_gap(&cmd_buf, i)) {
                    tmp[ix++] = cmd_buf.data[i];
                }
            }

            // Command parsing
            if (strcmp(tmp, "q") == 0) {  // quit
                running = false;
            }
            else if (strcmp(tmp, "") == 1) {
                printw("Command not found");
            }

            mode = NORMAL;
            cursor = old_cursor;
            break;
        case ESCAPE:
            mode = NORMAL;
            move(height - 1, 0);
            clrtoeol();
            cursor = old_cursor;
            break;
        case BACKSPACE:
            backspace(&cmd_buf);
            cmd_buf.length--;
            break;
        default:
            insert_char(&cmd_buf, key);
            cursor.x++;
            cmd_buf.length++;
            addch(key);

            break;
    }
}

void insert_mode(char key){
    switch(key) {
        case ESCAPE:
            mode = NORMAL;
            move(height - 1, 0);
            clrtoeol();
            if (arr_buf[cursor.y].length > 0) {
                cursor.x--;
            }
            break;
        case RETURN:
            // add line in between
            move(++cursor.y, 0);
            cursor.x = 0;
            arr_buf = realloc(arr_buf, ++lines * sizeof(*arr_buf));
            init_buffer(&arr_buf[lines-1]);
            break;
        case BACKSPACE:
            backspace(&arr_buf[cursor.y]);
            mvdelch(cursor.y, --cursor.x);
            arr_buf[cursor.y].length--;
            break;
        default:
            insert_char(&arr_buf[cursor.y], key);
            move(cursor.y, ++cursor.x);
            arr_buf[cursor.y].length++;
            break;
    }
}

int cursor_to_gaps(int cursor, GapBuffer *buf) {
    if (cursor < buf->gap_start) {
        return cursor;
    }
    return cursor + buf->gap_end - buf->gap_start + 1;
}
